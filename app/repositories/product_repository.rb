require 'mongo'

class ProductRepository
  def initialize()
    @client = Mongo::Client.new('mongodb://jarvis-mongo:27017/jarvis')
  end

  def deleteOne(name)
    collection = @client[:products]
    result = collection.delete_one( { name: name } )
    if result.deleted_count == 1
      return true
    else
      return false
    end
  end
end
