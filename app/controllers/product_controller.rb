require 'sinatra/base'
require_relative '../providers/container'

class ProductController < Sinatra::Base
  include AutoInject['productRepository']

  delete '/api/v1/products/:name' do
    if productRepository.deleteOne(params[:name])
      status 200
      body ''
    else
      status 409
      body ''
    end
  end
end