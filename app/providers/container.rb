require 'dry/system'
require 'dry/auto_inject'
require_relative '../repositories/product_repository'

dependency_container = Dry::Container.new
dependency_container.register('productRepository', -> {
  ProductRepository.new
})

AutoInject = Dry::AutoInject(dependency_container)